import csv
import requests
import sys

# Comando a ejecutar y parametros
# python script.py usuario pass ip 

class CheckpointWebServices():
    sid = None
    uri_login = None
    headers = {"content-Type": "application/json"}
    params = sys.argv                                       
      
    def __init__(self, *args, **kwargs):        
        self.user = sys.argv[1]
        self.password = sys.argv[2]
        self.ip = sys.argv[3]
        self.url =  "https://{0}/web_api/".format(self.ip)
        self.uri_login = "https://{0}/web_api/login".format(self.ip)
        
    
    def login(self):
        payload = {"user": self.user, "password": self.password }
        req = requests.post(self.uri_login, json=payload, verify=False)
        data = req.json()
        sid = data['sid']
        self.sid = sid

        return sid

    def PingPong(self):
        '''Esta funcion realiza una pruba de comunicacion e
           inicio de sesion al gateway               
        '''
        result = self.login
        print(self.params)

        return result
    
    def KeepAlive(self):
        '''Funion para  
        '''

    def publish(self):
        uri_publish = "{0}/publish".format(self.url)
        payload = {}
        headers = {'X-chkp-sid': self.sid}
        req = requests.post(uri_publish, json=payload, verify=False, headers=headers )

        return req.json()

    def show_hosts(self):
        uri_show_host = "{0}/show-hosts".format(self.url)
        sid = self.login()

        headers = {'X-chkp-sid': sid}

        payload = {
        "limit" : 100,
        "offset" : 0,
        "details-level" : "standard"
        }

        req = requests.post(
            uri_show_host,
            json=payload, 
            headers=headers, 
            verify=False
        )

        return req.json()

    def add_host(self, name, ipadd):
        uri_add_host = "{0}/add-host".format(self.url)
        sid = self.login()
        headers = {'X-chkp-sid': sid}
        payload = {
            'name': name, 
            'ip-address': ipadd
        }
        req = requests.post(uri_add_host,json=payload, headers=headers, verify=False)

        return req.json()

    def multiple_auto_add_obj(self):
        
        data_respon = list()
        with open('auto_add_object.csv') as f:
            reader = csv.reader(f)
            obje_fw = list(reader)

        for obj in obje_fw:
            if obj[0] == 'host':
                added = self.add_host(obj[1], obj[2])
                add_publish = self.publish()
                data_respon.append({ 
                    'name': obj[1],
                    'chkp-res': added,
                    'publish': add_publish
                })
               
            if obj[0] == 'net':
                print('net') 

        return data_respon

if __name__ == '__main__':

    test = CheckpointWebServices()

    print(test.multiple_auto_add_obj())
